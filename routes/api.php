<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::name('api.')->group(function () {
    Route::prefix('auth')->namespace('Auth')->name('auth.')->group(function () {
        Route::post('/register', 'AuthController@register')->name('register');
        Route::post('/login', 'AuthController@login')->name('login');
    });

    Route::prefix('auth')->namespace('Api')->group(function () {
        Route::get('users', 'UserController@index');
        Route::get('users/{id}', 'UserController@show');
        // Route::post('users', 'UserController@create');
        Route::put('users/{id}', 'UserController@update');
        Route::delete('users/{id}', 'UserController@destroy');

        Route::get('users/{id}/contas', 'ContaController@show');
        Route::post('users/{id}/contas', 'ContaController@create');
        Route::delete('contas/{id}', 'ContaController@destroy');

        Route::get('transacoes', 'TransacaoController@index');
        Route::post('transacoes', 'TransacaoController@create');
    });

    /*Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    })->name('user');*/
});
