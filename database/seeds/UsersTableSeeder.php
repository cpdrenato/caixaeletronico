<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Conta;
use App\Transacao;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::get()->count();
        if ($user == 0) {
            \DB::table('users')->insert(
                [
                    'name' => 'Renato',
                    'email' => 'cpdrenato@gmail.com',
                    'nascimento' => '2021-12-19',
                    'cpf' => '39849261030', // cpf fake
                    'email_verified_at' => now(),
                    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                ]
            );
            echo " Populating...";
            $usuario = factory('App\User', 10)->create();
            $conta = factory('App\Conta', 9)->create();
            $transacao = factory('App\Transacao', 3)->create();
        }
       
    }
}
