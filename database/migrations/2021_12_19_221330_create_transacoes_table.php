<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransacoesTable extends Migration
{
    public function up()
    {
        Schema::create('transacoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('tipo', ['deposito', 'saque']);
            $table->decimal('value', 10)->default(0);
            // $table->foreignId('conta_id')->constrained('contas');
            $table->unsignedBigInteger('conta_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('conta_id')->references('id')->on('contas');
        });
    }

    public function down()
    {
        Schema::dropIfExists('transacoes');
    }
}
