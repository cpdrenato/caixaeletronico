<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Transacao;
use Faker\Generator as Faker;

$factory->define(Transacao::class, function (Faker $faker) {
    $tipos = [Transacao::TIPO_DEPOSITO, Transacao::TIPO_SAQUE];
    return [
        'tipo' => $tipos[array_rand($tipos)],
        'value' => $this->faker->randomFloat(0, 10, 300),
        'conta_id' => $faker->randomDigitNotNull,
    ];
});
