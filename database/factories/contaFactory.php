<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Conta;
use Faker\Generator as Faker;

$factory->define(Conta::class, function (Faker $faker) {
    $tipos = [Conta::TIPO_CORRENTE, Conta::TIPO_POUPANCA];
    return [
        'tipo' => $tipos[array_rand($tipos)],
        'saldo' => $this->faker->randomFloat(2, 1000, 10000),
        'user_id' => $faker->unique()->randomDigitNotNull,
    ];

});


