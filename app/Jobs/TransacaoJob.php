<?php

namespace App\Jobs;

use App\Transacao;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransacaoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transacao;
    protected $attributes;

    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function handle(Transacao $transacao)
    {
        try {
            DB::beginTransaction();
            $transacao = $transacao->create($this->attributes)->fresh();
            $transacao->conta->saldo -= $this->attributes['value'];
            $transacao->push();

            DB::commit();
            Log::info('Transação criada com sucesso', $transacao->toArray());

        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception->getMessage());
        }

    }
}
