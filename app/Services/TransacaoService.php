<?php
namespace App\Services;

use App\Jobs\TransacaoJob;
use App\Transacao;
use App\Rules\Saldo;
use App\Rules\Retirada;
use Illuminate\Support\Facades\Validator;

class TransacaoService
{
    protected $model;
    protected $contaService;

    public function __construct(Transacao $transacao, ContaService $contaService)
    {
        $this->model = $transacao;
        $this->contaService = $contaService;
    }

    public function getByAccount(int $contaId)
    {
        // dump($contaId);
        return $this->model->getByAccount($contaId);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function createNew(array $attributes)
    {
        $this->validateStore($attributes);
        
        TransacaoJob::dispatch($attributes);
        return ["message" => "A transação foi enviada para a fila"];
    }

    protected function validateStore(array $attributes)
    {
        $deposito = Transacao::TIPO_DEPOSITO;
        $retirada = Transacao::TIPO_SAQUE;
        // dump($retirada);
        Validator::make($attributes, [
            'tipo' => "required|in:$deposito,$retirada",
            'value' => ["integer"],
            'conta_id' => 'required|exists:contas,id,deleted_at,NULL',
        ])->validate();
        // dump($attributes);
        $teste = Validator::make($attributes, [
            'value' => ["exclude_if:tipo,$deposito", new Retirada, new Saldo($this->contaService, $attributes['conta_id'])],
        ])->validate();
        dd($teste);

    }

}
