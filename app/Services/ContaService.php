<?php
namespace App\Services;

use App\Conta;
use Illuminate\Support\Facades\Validator;

class ContaService
{
    protected $model;

    public function __construct(Conta $conta)
    {
        $this->model = $conta;
    }

    public function getByUser(int $userId)
    {
        return $this->model->getByUser($userId);
    }

    public function createNewAccount(array $attributes, int $userId)
    {
        $attributes['user_id'] = $userId;
        $this->validateStore($attributes);
        return $this->model->create($attributes)->fresh();
    }

    protected function validateStore(array $attributes)
    {
        $corrente = Conta::TIPO_CORRENTE;
        $poupanca = Conta::TIPO_POUPANCA;

        Validator::make($attributes, [
            'tipo' => "required|in:$corrente,$poupanca",
            'saldo' => 'numeric',
            'user_id' => 'required|exists:users,id,deleted_at,NULL',
        ])->validate();
    }

    public function deleteById(int $id)
    {
        $object = $this->model->find($id);
        if ($object) {
            return $object->delete();
        }
        return false;
    }

    public function findById(int $id)
    {
        return $this->model->find($id);
    }
}
