<?php

namespace App;

use App\Conta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transacao extends Model
{
    use SoftDeletes;

    const TIPO_DEPOSITO = 'deposito';
    const TIPO_SAQUE = 'saque';

    protected $table = 'transacoes';

    protected $fillable = ['tipo', 'value', 'conta_id'];

    public function conta()
    {
        return $this->belongsTo(Conta::class);
    }

    public function findTrashedById(int $id)
    {
        return $this->onlyTrashed()->find($id);
    }

    public function getByAccount(int $contaId)
    {
        return $this->where(['conta_id' => $contaId])->get();
    }
}
