<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        return $this->userService->getAll();
    }

    public function show($id)
    {
        return $this->userService->findById($id);
    }

    /*public function create(Request $request)
    {
        $data = $request->only(['name', 'email', 'nascimento', 'cpf']);
        $user = $this->userService->createNew($data);
        return response()->json($user, 201);
    }*/

    public function update(Request $request, $id)
    {
        try {
            // dd($request->get('name'));
            $data = User::find($id)
                ->update([
                    'name' => $request->get('name'),
                    'nascimento' => $request->get('nascimento'),
                    'cpf' => $request->get('cpf'),
                ]);
            return response()->json($data, 200);
            
        } catch (\Exception $error) {
            \Log::error($error->getMessage());
            \Log::error($error->getTraceAsString());
            return response()->json($error->getMessage(), 500);
        }        
    }

    public function destroy($id)
    {
        $this->userService->deleteById($id);
        //return response()->noContent();
        return response()->json(['User deletado'], 200);
    }
}
