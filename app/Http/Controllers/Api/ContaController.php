<?php

namespace App\Http\Controllers\Api;

use App\Services\ContaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContaController extends Controller
{
    private $contaService;

    public function __construct(ContaService $contaService)
    {
        $this->contaService = $contaService;
    }

    public function show($userId)
    {
        return $this->contaService->getByUser($userId);
    }

    public function create(Request $request, $userId)
    {
        $data = $request->only(['tipo', 'saldo']);
        $conta = $this->contaService->createNewAccount($data, $userId);
        return response()->json($conta, 201);
    }

    public function destroy($id)
    {
        $this->contaService->deleteById($id);
        return response()->json(['Conta deletada'], 200);
    }
}
