<?php

namespace App\Http\Controllers\Api;

use App\Services\TransacaoService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransacaoController extends Controller
{
    private $transacaoService;

    public function __construct(TransacaoService $transacaoService)
    {
        $this->transacaoService = $transacaoService;
    }

    public function index(Request $request)
    {
        $contaId = $request->get('conta_id');
        if ($contaId) {
            return $this->transacaoService->getByAccount($contaId);
        }

        return $this->transacaoService->getAll();
    }

    public function create(Request $request)
    {
        $data = $request->only(['tipo', 'value', 'conta_id']);
        $transacao = $this->transacaoService->createNew($data);
        return response()->json($transacao, 202);
    }

}
