<?php

namespace App\Rules;

use App\Conta;
use App\Services\ContaService;
use Illuminate\Contracts\Validation\Rule;

class Saldo implements Rule
{
    private $conta;

    public function __construct(ContaService $contaService, $contaId)
    {
        $this->conta = $contaService->findById($contaId);
    }

    public function passes($attribute, $value)
    {
        if (!empty($this->conta) && $this->conta->saldo >= $value) {
            return true;
        }
        return false;
    }

    public function message()
    {
        return 'Saldo insuficiente para realizar a transação';
    }
}
