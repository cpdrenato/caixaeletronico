<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Retirada implements Rule
{
    public function passes($attribute, $amount)
    {
        $moneyBills = [100, 50, 20];
        $remainder = $amount;
        
        foreach ($moneyBills as $moneyBill) {
            if ($amount % $moneyBill == 0) {
                $remainder = 0;
                break;
            }

            $remainder = $remainder % $moneyBill;
            
        }
        // dump(empty($remainder));
        if (empty($remainder) == false){
            return $this->message();
        }
        return empty($remainder);
    }

    public function message()
    {
        \Log::error('Não temos nota de dinheiro disponível para sacar este valor. Conta em dinheiro disponível: 100, 50 e 20');
        return 'Não temos nota de dinheiro disponível para sacar este valor. Conta em dinheiro disponível: 100, 50 e 20';
    }
}
