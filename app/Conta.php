<?php

namespace App;

use App\Transacao;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conta extends Model
{
    use SoftDeletes;
    
    const TIPO_CORRENTE = 'corrente';
    const TIPO_POUPANCA = 'poupanca';

    protected $table = 'contas';
    protected $fillable = ['tipo', 'saldo', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transacao()
    {
        return $this->hasMany(Transacao::class);
    }

    public function findTrashedById(int $id)
    {
        return $this->onlyTrashed()->find($id);
    }

    public function getByUser($userId)
    {
        return $this->where(['user_id' => $userId])->get();
    }

}
