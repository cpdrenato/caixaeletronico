# Desafio Backend Linx Digital

## Teste Simulador Caixa Eletrônico

### Desenvolver APIs simulando operações de um caixa eletrônico com os seguintes requisitos:

1. Possibilidade de cadastrar, alterar, excluir e buscar usuários. Os atributos para
usuário precisam ser nome, data de nascimento e cpf.
2. Possibilidade de cadastrar contas para usuários com tipo da conta (poupança ou
corrente) e saldo.
3. O usuário poderá solicitar extrato de movimentação com as informações de
operações: Tipo de operação “saque, depósito, etc”, valor da operação, data e hora
da operação. Os extratos deverão estar em cache.
4. O usuário poderá fazer depósito de qualquer valor em sua conta, exceto centavos.
5. O usuário poderá realizar o saque de sua conta apenas utilizando as notas de 20, 50
ou 100.
a. A API de saque deverá priorizar as notas maiores para compor o valor total.
Exemplo: Se escolher sacar 150, então liberar uma nota de 100 e outra de 50.
Se escolher sacar 60, então deverá liberar 3 notas de 20.
b. Se o valor solicitado para saque for maior que o disponível na conta do usuário,
exibir erro.
c. Se não houver cédulas disponíveis para o valor solicitado, exibir erro. Por
exemplo, se for solicitado o saque de 15, não será possível seguir com a
operação visto que a nota mínima de saque é 20.
d. Se o saque for 30 por exemplo, a API deverá retornar um erro dizendo que não é
possível realizar o saque
6. Desenhar a solução pensando em escalabilidade.
7. Implementar a solução utilizando a linguagem de programação PHP

## Como entregar ?
Crie um repositório na sua conta do github e compartilhe conosco

## O que é esperado neste teste?

- Uso de docker
- Testes automatizados de alguns fluxos
- Uso de banco de dados
- Uso dos padrões de API REST (verbos, endpoints, status code, etc)
- Documentação de como executar a aplicação
- Documentação de como usar os endpoints
- Uso de git, o código poderá estar no Github ou Bitbucket

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# O que foi feito e usado

## laravel-firebase-jwt
- https://packagist.org/packages/firebase/php-jwt

## Docker alpine
- MySQL 8 (https://www.mysql.com/)
- PHP 7.2 (https://www.php.net/)
- Laravel 6.0 (https://lumen.laravel.com/)
- Docker (https://www.docker.com/)
- https://docs.docker.com/compose/install/
- Nginx (https://www.nginx.com/)

## Instalação
Você pode rodar esse projeto usando o Docker Compose
```sh
$ docker-compose up  -d
```
A instalação das dependências, criação do .env, execução dos migrations e seed são feitas automaticamente. 

## Logs 

```sh
> $ docker logs desafio.php
```

Página da aplicação http://localhost:81/ 

## Endpoints
### Users
- `GET users`, lista todos os usuários
- `GET users/{id}`, mostra detalhe de usuário específico
- `POST users`, cadastra usuário
- `PUT users/{id}`, atualiza usuário
- `DELETE users/{id}`, deleta usuário

### Contas
- `GET users/{id}/contas` lista contas do usuário
- `POST users/{id}/contas` cadastra contas
- `DELETE users/{id}/contas/{id}` exclui conta

### Transacoes
- `GET transacoes` lista todas as movimentações
- `GET transacoes?account=id` lista as movimentações da conta
- `POST transacoes` fazer uma transação (deposito ou saque)

> Anexo ao projeto está o JSON do Postaman para replicarem
- Api-Desafio.postman_collection.json

Renato Lucena - 12/2021